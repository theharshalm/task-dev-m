trigger AccountTriggerNoOfChildAccounts on Account (after insert, after update, after delete, after undelete) {
	if(Trigger.isInsert){
        if(Trigger.isAfter){
            AccountTriggerNoOfChildAccountsHandler.autofillNumberOfChildAccountsOnInsertUndelete(Trigger.new);  
        }
    }
    
    if(Trigger.isUpdate){
        if(Trigger.isAfter){
            AccountTriggerNoOfChildAccountsHandler.autofillNumberOfChildAccountsOnUpdate(Trigger.new, Trigger.oldMap);
        }
    }
    
    if(Trigger.isDelete){
        if(Trigger.isAfter){
            AccountTriggerNoOfChildAccountsHandler.autofillNumberOfChildAccountsOnDelete(Trigger.old);
        }
    }
    
    if(Trigger.isUndelete){
        if(Trigger.isAfter){
            AccountTriggerNoOfChildAccountsHandler.autofillNumberOfChildAccountsOnInsertUndelete(Trigger.new);
        }
    }
}