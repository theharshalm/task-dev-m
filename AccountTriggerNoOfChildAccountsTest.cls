@isTest
public class AccountTriggerNoOfChildAccountsTest {
    @testSetup
    static void testData(){ 
        Account parentAccount = new Account(Name = 'Demo Parent Account');
        insert parentAccount;
        List<Account> lstParentId = [SELECT ID FROM Account WHERE Name = 'Demo Parent Account'];
        Id parentAccountId = lstParentId[0].Id;
        
        Integer numOfAccounts = 50;
        List<Account> accountList = new List<Account>();
        for(Integer index=1; index <= numOfAccounts; index++){
            Account account = new Account();
            account.Name = 'Demo Account '+ index;
            accountList.add(account);
        }
        insert accountList;
        
        List<Account> lstChildAccounts = [SELECT Id, ParentId FROM Account WHERE Name LIKE 'Demo Account %'];
        for(Account childAccount :lstChildAccounts){
            childAccount.ParentId = parentAccountId;
        }
        update lstChildAccounts;
    }

    @isTest
    static void testAutofillNumberOfChildAccountsInsert(){
        Test.startTest();
        List<Account> parentAccount = [SELECT Id,Number_Of_Child_Accounts__c, (SELECT Id FROM ChildAccounts) FROM Account
                                       WHERE Name = 'Demo Parent Account'];
        System.assertEquals(parentAccount[0].Number_Of_Child_Accounts__c,parentAccount[0].ChildAccounts.size());
        Test.stopTest();
    }
    
    @isTest
    static void testAutofillNumberOfChildAccountsUpdate(){
        Test.startTest();
        List<Account> lstParentAccounts = [SELECT Id,Number_Of_Child_Accounts__c, (SELECT Id FROM ChildAccounts) 
                                           FROM Account WHERE Name = 'Demo Parent Account'];
        System.assertEquals(lstParentAccounts[0].Number_Of_Child_Accounts__c,lstParentAccounts[0].ChildAccounts.size());
        Test.stopTest();
    }
    
    @isTest
    static void testAutofillNumberOfChildAccountsOnDelete(){
        Test.startTest();
        List<Account> lstChildAccounts = [SELECT Id FROM Account WHERE Name LIKE 'Demo Account %'];
        delete lstChildAccounts;					// Hits Delete Trigger, and Update NumberOfChild 15 Only.
        
        List<Account> lstParentAccounts = [SELECT Id,Number_Of_Child_Accounts__c, (SELECT Id FROM ChildAccounts) 
                                           FROM Account WHERE Name = 'Demo Parent Account'];
        System.assertEquals(lstParentAccounts[0].Number_Of_Child_Accounts__c,lstParentAccounts[0].ChildAccounts.size());
        
        List<Account> lstDeletedChildAccounts = [SELECT Id FROM Account WHERE Name LIKE 'Demo Account %' ALL ROWS];
        System.debug(lstDeletedChildAccounts.size());
        undelete lstDeletedChildAccounts;
        
        List<Account> lstAfterUndeleteParentAccounts = [SELECT Id,Number_Of_Child_Accounts__c, (SELECT Id FROM ChildAccounts) 
                                           				FROM Account WHERE Name = 'Demo Parent Account'];
        System.debug(lstAfterUndeleteParentAccounts[0].Number_Of_Child_Accounts__c);
        System.debug(lstAfterUndeleteParentAccounts[0].ChildAccounts.size());
        System.assertEquals(lstAfterUndeleteParentAccounts[0].Number_Of_Child_Accounts__c,
                            lstAfterUndeleteParentAccounts[0].ChildAccounts.size());
        Test.stopTest();
    }
}
