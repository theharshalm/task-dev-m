public class AccountTriggerNoOfChildAccountsHandler {
    // Q : Autopopulate Number_Of_Child_Accounts Feild on After Insert/Undelete.
    public static void autofillNumberOfChildAccountsOnInsertUndelete(List<Account> lstNewAccounts){       
        Set<Id> parentAccountIds = new Set<Id>();
        for(Account account : lstNewAccounts) {
            if(account.ParentId != null) {
                parentAccountIds.add(account.ParentId);
            }
        }
        
        Map<Id, Account> parentAccounts = new Map<Id, Account>([SELECT Id, (SELECT Id FROM ChildAccounts) 
                                                                FROM Account WHERE Id IN :parentAccountIds]);
        
        for(Account parentAccount : parentAccounts.values()) {
            parentAccount.Number_of_Child_Accounts__c = parentAccount.ChildAccounts.size();
        }
        update parentAccounts.values();
     }
    
    public static void autofillNumberOfChildAccountsOnUpdate(List<Account> lstNewAccount, Map<Id,Account> mapOldAccounts){
        Set<Id> parentAccountIds = new Set<Id>();
        for(Account newAccount : lstNewAccount){
            Account oldAccount = mapOldAccounts.get(newAccount.Id);
            if(newAccount.ParentId != oldAccount.ParentId){
                parentAccountIds.add(newAccount.ParentId);
            }
        }
        
        Map<Id, Account> mapParentAccounts = new Map<Id, Account>([SELECT Id, (SELECT Id FROM ChildAccounts) 
                                                                FROM Account WHERE Id IN :parentAccountIds]);
        for(Account parentAccount : mapParentAccounts.values()) {
            parentAccount.Number_of_Child_Accounts__c = parentAccount.ChildAccounts.size();
        }
        update mapParentAccounts.values();
    }
    
    public static void autofillNumberOfChildAccountsOnDelete(List<Account> lstDeletedAccounts){
        // Getting Parent AccountId whose Child Accounts are Deleted.
        Set<Id> setParentIds = new Set<Id>();
        for(Account delAccount : lstDeletedAccounts){
            if(delAccount.ParentId != null){
                setParentIds.add(delAccount.ParentId);
            }
        }
        // Updating Parent's Number of Child Feild.
        Map<Id, Account> mapParentAccounts = new Map<Id, Account>([SELECT Id, (SELECT Id From ChildAccounts) 
                                                                   FROM Account WHERE Id In : setParentIds]);
        for(Account parentAccount : mapParentAccounts.values()){
            parentAccount.Number_Of_Child_Accounts__c = parentAccount.ChildAccounts.Size();
        }
        update mapParentAccounts.values();
    }

}